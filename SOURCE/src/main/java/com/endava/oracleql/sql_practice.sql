-- Write a query to display:
-- 1. the first name, last name, department number, and department name for each employee.
select FIRST_NAME, LAST_NAME, DEPARTMENT_ID, DEPARTMENT_NAME
from EMPLOYEES E left join DEPARTMENTS D using(DEPARTMENT_ID);

-- 2. the first and last name, department, city, and state province for each employee.
select e.first_name, e.last_name, d.department_name, l.city, l.state_province
from EMPLOYEES e left join departments d on e.department_id = d.DEPARTMENT_ID
                  left join locations l on d.location_id = l.location_id;

-- 3. the first name, last name, salary, and job grade for all employees.
select first_name, last_name, salary, job_title
from EMPLOYEES left join JOBS using(job_id);

-- 4. the first name, last name, department number and department name, for all employees for departments 80 or 40.
select e.first_name, e.last_name, d.department_id, d.department_name
from EMPLOYEES e left join DEPARTMENTS D on e.department_id = d.department_id
where d.department_id = 80 or d.department_id = 40;

-- 5. those employees who contain a letter z to their first name and also display their last name, department, city, and state province.
select e.first_name, e.last_name, d.department_name, l.city, l.state_province
from EMPLOYEES e left join departments d on e.department_id = d.DEPARTMENT_ID
                  left join locations l on d.location_id = l.location_id
                  where e.first_name like '%z%';

-- 6. all departments including those where does not have any employee.
select e.first_name, e.last_name, d.department_id, d.department_name
from EMPLOYEES e right join DEPARTMENTS D on e.department_id = d.department_id;

-- 7. the first and last name and salary for those employees who earn less than the employee earn whose number is 182.
select e.first_name, e.last_name, e.SALARY
from EMPLOYEES e
join EMPLOYEES m
on e.salary < m.salary
and m.EMPLOYEE_ID = 182;

-- 8. the first name of all employees including the first name of their manager.
select e.first_name, m.first_name
from EMPLOYEES e left join DEPARTMENTS d using (department_ID)
                 join EMPLOYEES m on e.manager_id = m.manager_id;

-- 9. the department name, city, and state province for each department.
select d.department_name, l.city, l.state_province
from DEPARTMENTS d left join LOCATIONS l using (location_id);

--10. the first name, last name, department number and name, for all employees who have or have not any department.
select e.first_name, e.last_name, d.department_id, d.department_name
from EMPLOYEES e left join DEPARTMENTS d on e.department_id = d.DEPARTMENT_ID;

--11. the first name of all employees and the first name of their manager including those who does not working under any manager.
select e.first_name, m.first_name as manager_name
from EMPLOYEES e left join DEPARTMENTS d using (department_ID)
                 join EMPLOYEES m on e.manager_id = m.manager_id
                 order by e.first_name;

--12. the first name, last name, and department number for those employees who works in the same department as the employee who holds the last name as Taylor.
select e.first_name, e.last_name, e.department_id
from EMPLOYEES e join EMPLOYEES m on e.department_id = m.department_id
and m.last_name = 'Taylor';

--13. the job title, department name, full name (first and last name ) of employee, and starting date for all the jobs which started on or after 1st January, 1993 and ending with on or before 31 August, 1997.
select job_title, department_name, first_name, last_name, start_date
from EMPLOYEES e join DEPARTMENTS d using (DEPARTMENT_ID)
                 join JOBS j using (job_id)
                 join JOB_HISTORY h using (EMPLOYEE_ID)
where h.START_DATE >= '01-JAN-1993' and h.END_DATE <= '31-JAN-1997';
--14. job title, full name (first and last name ) of employee, and the difference between maximum salary for the job and salary of the employee.
select job_title, first_name||' '||last_name as full_name, MAX_SALARY-salary as job_salary
from EMPLOYEES e join JOBS j using (job_id);

--15. the name of the department, average salary and number of employees working in that department who got commission.
select department_name, avg(salary), count(commission_pct)
from DEPARTMENTS d join EMPLOYEES e using (department_id)
group by department_name;
-------------------------------- revise it ----------------------

--16. the full name (first and last name ) of employee, and job title of those employees who is working in the department which ID is 80.
select first_name||' '||last_name as full_name, job_title
from EMPLOYEES e join JOBS j using (job_id)
where e.DEPARTMENT_ID = 80;

--17. the name of the country, city, and the departments which are running there.
select country_name, city, department_name
from COUNTRIES c join LOCATIONS l using (country_id)
                 join DEPARTMENTS d using (location_id);

--18. department name and the full name (first and last name) of the manager.
select department_name, first_name||' '||last_name as full_name
from DEPARTMENTS d join EMPLOYEES e on d.manager_id = e.employee_id;

--19. job title and average salary of employees.
select job_title, avg(salary)
from JOBS j join EMPLOYEES e using (job_id)
group by job_title;

--20. the details of jobs which was done by any of the employees who is presently earning a salary on and above 12000.
select job_title, min_salary, max_salary
from EMPLOYEES e join JOB_HISTORY h on e.job_id = h.job_id
            join JOBS j on h.job_id = j.job_id
where e.salary > 12000;

--21. the country name, city, and number of those departments where at least 2 employees are working.
select country_name, city, count(department_id)
from DEPARTMENTS d join LOCATIONS l using (location_id)
                   join COUNTRIES c using (country_id)
where department_id in
  ( select department_id
    from EMPLOYEES e
    group by department_id
    having count(department_id) >= 2
  )
group by country_name, city;

--22. the department name, full name (first and last name) of manager, and their city.
select department_name, first_name||' '||last_name as full_name, city
from DEPARTMENTS d join EMPLOYEES e on d.manager_id = e.employee_id
                   join LOCATIONS l using (location_id);

--23. the employee ID, job name, number of days worked in for all those jobs in department 80.
select employee_id, JOB_TITLE, (end_date - start_date) as nr_days
from JOB_HISTORY join JOBS using (job_id)
where department_id = 80;

--24. the full name (first and last name), and salary of those employees who working in any department located in London.
select first_name||' '||last_name as full_name, salary
from EMPLOYEES e join DEPARTMENTS using (DEPARTMENT_ID)
                 join LOCATIONS using (LOCATION_ID)
                 where CITY = 'London';

--25. full name(first and last name), job title, starting and ending date of last jobs for those employees with worked without a commission percentage.
select first_name||' '||last_name as full_name, job_title, start_date, end_date
from EMPLOYEES e join JOBS j using (JOB_ID)
                 join JOB_HISTORY h using (JOB_ID)
                 where e.COMMISSION_PCT = null;

--26. the department name and number of employees in each of the department.
select department_name, count(department_id) nr_employees_per_department
from EMPLOYEES e right join DEPARTMENTS d using (DEPARTMENT_ID)
group by DEPARTMENT_NAME;

--27. the full name (first and last name ) of employee with ID and name of the country presently where (s)he is working.
select first_name||' '||last_name as full_name, country_id, country_name
from EMPLOYEES e left join DEPARTMENTS d using (DEPARTMENT_ID)
                 join LOCATIONS l using (LOCATION_ID)
                 join COUNTRIES c using (COUNTRY_ID);

--28. the name ( first name and last name ) for those employees who gets more salary than the employee whose ID is 163.
select e.first_name||' '||e.last_name as full_name, e.salary
from EMPLOYEES e join EMPLOYEES m on e.salary > m.salary
and m.employee_id = 163;

--29. the name ( first name and last name ), salary, department id, job id for those employees who works in the same designation as the employee works whose id is 169.
select e.first_name||' '||e.last_name as full_name, e.salary, e.department_id, e.JOB_ID
from EMPLOYEES e join JOBS j on e.JOB_ID = j.JOB_ID
where e.JOB_ID = (
        select job_id
        from EMPLOYEES
        where EMPLOYEE_ID = 169
    );

--30. the name ( first name and last name ), salary, department id for those employees who earn such amount of salary which is the smallest salary of any of the departments.
select first_name||' '||last_name as full_name, salary, department_id
from EMPLOYEES
where SALARY IN (select min(salary) from EMPLOYEES
                group by department_id);

--31. the employee id, employee name (first name and last name ) for all employees who earn more than the average salary.
select employee_id, first_name||' '||last_name as employee_name
from EMPLOYEES e
where salary > (select avg(SALARY) from EMPLOYEES);

--32. the employee name ( first name and last name ), employee id and salary of all employees who report to Payam.
select first_name||' '||last_name as employee_name, employee_id, salary
from EMPLOYEES e join DEPARTMENTS d using (DEPARTMENT_ID)
                 join LOCATIONS l using (LOCATION_ID)
                 where city = 'Payam';
--33. the department number, name ( first name and last name ), job and department name for all employees in the Finance department.
select department_id, first_name||' '||last_name as name, job_title, department_name
from JOBS j join EMPLOYEES e using (JOB_ID)
            join DEPARTMENTS d using (DEPARTMENT_ID)
where JOB_TITLE = 'Finance';

--34. all the information of an employee whose salary and reporting person id is 3000 and 121 respectively.
select * from EMPLOYEES
where salary = 3000 and employee_id = 121;

--35. all the information of an employee whose id is any of the number 134, 159 and 183.
select * from EMPLOYEES
where EMPLOYEE_ID = 134 or employee_id = 159 or EMPLOYEE_ID = 183;

--36. all the information of the employees whose salary is within the range 1000 and 3000.
select * from EMPLOYEES
where SALARY between 1000 and 3000;

--37. all the information of the employees whose salary is within the range of smallest salary and 2500.
select  * from EMPLOYEES e
where SALARY between (select min(SALARY) from EMPLOYEES ) and 2500;

--38. all the information of the employees who does not work in those departments where some employees works whose id within the range 100 and 200.
select * from EMPLOYEES
where EMPLOYEE_ID not between 100 and 200;

--39. all the information for those employees whose id is any id who earn the second highest salary.
select * from employees e
where employee_id in (select employee_id
                      from employees
                      where 2 = (select count(distinct salary) from EMPLOYEES
                                    where salary >= e.SALARY));

--40. the employee name( first name and last name ) and hire date for all employees in the same department as Clara. Exclude Clara.
select first_name||' '||last_name as employee_name, hire_date
from EMPLOYEES
where DEPARTMENT_ID =
    (select department_id from EMPLOYEES
    where FIRST_NAME = 'Clara'
    );

--41. the employee number and name( first name and last name ) for all employees who work in a department with any employee whose name contains a T.
select employee_id, first_name||' '||last_name as name
from EMPLOYEES
where DEPARTMENT_ID in (
    select department_id from EMPLOYEES
    where FIRST_NAME like '%T%' or LAST_NAME like '%T%'
);

--42. the employee number, name( first name and last name ), and salary for all employees who earn more than the average salary and who work in a department with any employee with a J in their name.
select employee_id, first_name||' '||last_name as name, salary
from EMPLOYEES e
where  salary > (select avg(salary) from EMPLOYEES)
and DEPARTMENT_ID in (
    select department_id from EMPLOYEES
    where FIRST_NAME like '%J%' or LAST_NAME like '%J%'
)

--43. the employee name( first name and last name ), employee id, and job title for all employees whose department location is Toronto.
select first_name||' '||last_name as name, employee_id, job_title
from EMPLOYEES e join JOBS j using (JOB_ID)
where DEPARTMENT_ID in (
    select department_id
    from DEPARTMENTS e join LOCATIONS l using (LOCATION_ID)
    where CITY = 'Toronto'
);

--44. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN.
select e.employee_id, e.first_name||' '||e.last_name as name, j.job_id
from EMPLOYEES e left join JOBS j on e.JOB_ID = j.JOB_ID
                 join EMPLOYEES m on e.EMPLOYEE_ID = m.EMPLOYEE_ID
where j.JOB_ID = 'MK_MAN';

--45. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN. Exclude Job title MK_MAN.
select e.employee_id, e.first_name||' '||e.last_name as name, e.job_ID
from EMPLOYEES e join EMPLOYEES m on e.EMPLOYEE_ID = m.EMPLOYEE_ID
where e.JOB_ID = 'MK_MAN' except e.job_ID = 'MK_MAN';

--46. all the information of those employees who did not have any job in the past
select  * from EMPLOYEES
where EMPLOYEE_ID not in (
    select employee_id from JOB_HISTORY
   );

--47. the employee number, name( first name and last name ) and job title for all employees whose salary is more than any average salary of any department.
select e.employee_id, e.first_name||' '||e.last_name as name, j.job_title
from EMPLOYEES e join JOBS j using (JOB_ID)
where SALARY > (
    select avg(salary) from EMPLOYEES
    where DEPARTMENT_ID in (
        select department_id from EMPLOYEES
        group by DEPARTMENT_ID
    )
);

--48. the employee name( first name and last name ) and department for all employees for any existence of those employees whose salary is more than 3700.
select e.first_name||' '||e.last_name as employee_name, department_name
from employees e join departments d using (department_id)
where e.salary > 3700;

--49. the department id and the total salary for those departments which contains at least one salaried employee.
select department_id, sum(salary) as total_salary
from employees e where salary <> 0
group by department_id;

--50. the employee id, name ( first name and last name ) and the job id column with a modified title SALESMAN for those employees whose job title is ST_MAN and DEVELOPER for whose job title is IT_PROG.
select employee_id, first_name||' '||last_name as name,
case job_id
    when 'ST_MAN' then 'SALESMAN'
    when 'IT_PROG' then 'DEVELOPER'
    else job_id
end as jobID
from employees e;

--51. the employee id, name ( first name and last name ), salary and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than the average salary of all employees.
select employee_id, first_name||' '||last_name as name, salary,
 case
    when salary >= (select avg(salary)from employees) then 'HIGH'
    when salary <= (select avg(salary)from employees) then 'LOW'
 end as SalaryStatus
from employees e;

--52. the employee id, name ( first name and last name ), SalaryDrawn, AvgCompare (salary - the average salary of all employees) and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less than the average salary of all employees.
select employee_id, first_name||' '||last_name as name,
(salary - 1000) as SalaryDrawn,
 (salary - (select avg(salary) from hr.employees)) as AvgCompare,
 case
    when salary >= (select avg(salary)from hr.employees) then 'HIGH'
    when salary <= (select avg(salary)from hr.employees) then 'LOW'
 end as SalaryStatus
 from hr.employees e;

--53. a set of rows to find all departments that do actually have one or more employees assigned to them.
select distinct d.department_name, e.employee_id
from hr.employees e join hr.departments d using (department_id);

--54. all employees who work in departments located in the United Kingdom.
select employee_id, first_name||' '||last_name as name
from hr.employees e left join hr.departments d using (department_id)
                                  join hr.locations l using (location_id)
                                  join hr.countries c using (country_id)
where country_name = 'United Kingdom';

--55. all the employees who earn more than the average and who work in any of the IT departments.
select employee_id, first_name||' '||last_name as name
from hr.employees e join hr.departments d using (department_id)
where salary > (select avg(salary) from hr.employees e)
and department_name like 'IT%';

--56. who earns more than Mr. Ozer.
select employee_id, first_name||' '||last_name as name
from hr.employees
where salary > (select salary from hr.employees
                 where last_name = 'Ozer'
);

--57. which employees have a manager who works for a department based in the US.
select first_name||' '||last_name as name
from hr.employees e join hr.departments d on e.manager_id = d.manager_id
where e.department_id in (select department_id from hr.departments
                                             join hr.locations l using (location_id)
                                             join hr.countries c using (country_id)
                        where country_id = 'US');

--58. the names of all employees whose salary is greater than 50% of their departmentâ€™s total salary bill.
select first_name||' '||last_name as name
from hr.employees e
where e.salary > (50/100 * (select sum(salary) from hr.employees m
                    where e.department_id = m.department_id));

--59. the details of employees who are managers.
select *
from hr.employees e
where exists (select * from hr.departments d
                where d.manager_id = e.employee_id);

--60. the details of employees who manage a department.
select * from hr.employees
where employee_id = any (select manager_id from hr.departments);

--61. the employee id, name ( first name and last name ), salary, department name and city for all the employees who gets the salary as the salary earn by the employee which is maximum within the joining person January 1st, 2002 and December 31st, 2003.
select employee_id, first_name||' '||last_name as name, salary, department_name, city
from hr.employees join hr.departments using (department_id)
                  join hr.locations using (location_id)
where salary = (select max(salary) from hr.employees
                            where hire_date between '01-JAN-2002' and '31-DEC-2003');

--62. the department code and name for all departments which located in the city London.
select department_id, department_name
from hr.departments join hr.locations using (location_id)
where city = 'London';

--63. the first and last name, salary, and department ID for all those employees who earn more than the average salary and arrange the list in descending order on salary.
select first_name, last_name, salary, department_id
from hr.employees
where salary > (select avg(salary) from hr.employees)
order by salary desc;

--64. the first and last name, salary, and department ID for those employees who earn more than the maximum salary of a department which ID is 40.
select first_name, last_name, salary, department_id
from hr.employees
where salary > (select max(salary) from hr.employees
                where department_id = 40);

--65. the department name and Id for all departments where they located, that Id is equal to the Id for the location where department number 30 is located.
select department_name, department_id
from hr.departments
where location_id = (select location_id from hr.departments where department_id = 30);

--66. the first and last name, salary, and department ID for all those employees who work in that department where the employee works who hold the ID 201.
select first_name, last_name, salary, department_id
from hr.employees
where department_id = (select department_id from hr.employees where employee_id = 201);

--67. the first and last name, salary, and department ID for those employees whose salary is equal to the salary of the employee who works in that department which ID is 40.
select first_name, last_name, salary, department_id
from hr.employees
where salary in (select salary from hr.employees
                where department_id = 40);

--68. the first and last name, and department code for all employees who work in the department Marketing.
select first_name, last_name, department_id
from hr.employees join hr.departments using (department_id)
where department_name = 'Marketing';

--69. the first and last name, salary, and department ID for those employees who earn more than the minimum salary of a department which ID is 40.
select first_name, last_name, salary, department_id
from hr.employees
where salary > (select min(salary) from hr.employees where department_id = 40
);

--70. the full name,email, and designation for all those employees who was hired after the employee whose ID is 165.
select first_name||' '||last_name as full_name, email, department_name as designation
from hr.employees join hr.departments using (department_id)
where hire_date > (select hire_date from hr.employees where employee_id = 165);

--71. the first and last name, salary, and department ID for those employees who earn less than the minimum salary of a department which ID is 70.
select first_name, last_name, salary, department_id
from hr.employees
where salary < (select min(salary) from hr.employees where department_id = 70);

--72. the first and last name, salary, and department ID for those employees who earn less than the average salary, and also work at the department where the employee Laura is working as a first name holder.
select first_name, last_name, salary, department_id
from hr.employees
where salary < (select avg(salary) from hr.employees)
and department_id = (select department_id from hr.employees where first_name = 'Laura');

--73. the city of the employee whose ID 134 and works there.
select city
from hr.locations join hr.departments using (location_id)
where department_id = (select department_id from hr.employees where employee_id = 134);

--74. the the details of those departments which max salary is 7000 or above for those employees who already done one or more jobs.
select * from hr.departments
where department_id in (select department_id from hr.employees
                        where (select max(salary) from hr.employees) >= 7000
                        and job_id not in (select job_id from hr.job_history)
                        group by department_id
                        );

--75. the detail information of those departments which starting salary is at least 8000.
select * from departments
where department_id in (select department_id from employees where salary > 8000);

--76. the full name (first and last name) of manager who is supervising 4 or more employees.
select e.first_name||' '||e.last_name as full_name
from EMPLOYEES e join EMPLOYEES m on e.MANAGER_ID = m.EMPLOYEE_ID
where (select count(e.MANAGER_ID) from EMPLOYEES) > 4;

--77. the details of the current job for those employees who worked as a Sales Representative in the past.
select * from jobs
where JOB_ID in (
    select job_id from EMPLOYEES
    where EMPLOYEE_ID in (
        select EMPLOYEE_ID from JOB_HISTORY h join jobs j using (JOB_ID)
        where j.JOB_TITLE = 'Sales Representative'
    ));

--78. all the infromation about those employees who earn second lowest salary of all the employees.
select  * from EMPLOYEES e
where 2 = (select count(distinct salary) from EMPLOYEES
                where salary <= e.SALARY
                );

--79. the details of departments managed by Susan.
select * from DEPARTMENTS
where MANAGER_ID = (select EMPLOYEE_ID from EMPLOYEES
                    where first_NAME = 'Susan'
                    );

--80. the department ID, full name (first and last name), salary for those employees who is highest salary drawar in a department.
select department_id, first_name||' '||last_name as full_name, salary
from EMPLOYEES e
where SALARY = (select max(salary) from EMPLOYEES
                where DEPARTMENT_ID = e.DEPARTMENT_ID);
