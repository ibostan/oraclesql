alter table assignments
add constraint emp_fk foreign key (employee_id) references employees (employee_id);

alter table assignments
add constraint proj_fk foreign key (project_id) references projects (project_id);