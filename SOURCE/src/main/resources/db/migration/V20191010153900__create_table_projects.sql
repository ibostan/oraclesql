create table projects(
    project_id NUMBER primary key,
    project_description VARCHAR(250),
    project_investment NUMBER(12,-3),
    project_revenue NUMBER,
    assignment_id NUMBER,
    constraint assign_fk foreign key (assignment_id) references assignments (assignment_id),
    constraint proj_inv  check (project_investment > 0)
);

