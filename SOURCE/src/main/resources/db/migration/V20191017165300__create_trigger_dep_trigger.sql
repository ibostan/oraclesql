create or replace trigger dep_trigger
 AFTER
    INSERT OR DELETE
    ON departments
    FOR EACH ROW
 DECLARE
   l_transaction VARCHAR2(10);
BEGIN
   l_transaction := CASE
         WHEN INSERTING THEN 'INSERT'
         WHEN DELETING THEN 'DELETE'
   END;
   update locations set department_amount = department_amount + 1
   where l_transaction = 'INSERT';
   update  locations set department_amount = department_amount - 1
   where l_transaction = 'DELETE';
   end;

