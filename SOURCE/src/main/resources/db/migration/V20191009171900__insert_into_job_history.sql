insert into REGIONS values
(1, 'Central Europe');

insert into COUNTRIES values
(1, 'Moldova', 1);

insert into LOCATIONS values
(1, 'Arborelui 21 Street', 2420, 'Chisinau', 'Municipiul Chisinau', 1);

insert into DEPARTMENTS values
(1, 'Higher Management', null, 1);

insert into JOBS values
(1, 'President', 23000, 24000);

INSERT into employees values
(1, 'Jackson', 'Mary', 'mary.jackson@gmail.com', 293345678, '09-JAN-2012', 1, 23000, null, null, 1);

INSERT INTO JOB_HISTORY VALUES (
1, '09-JAN-2012', '10-OCT-2019', 1, 1);