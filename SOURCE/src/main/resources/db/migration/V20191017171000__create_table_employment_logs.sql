create table employment_logs (
    employment_id NUMBER(6) PRIMARY KEY,
    first_name VARCHAR(20),
    last_name VARCHAR(25),
    employment_action VARCHAR(10) NOT NULL,
    employment_status_timestamp date
);