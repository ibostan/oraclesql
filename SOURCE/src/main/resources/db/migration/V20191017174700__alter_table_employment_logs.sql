alter table employment_logs
add constraint emp_action check (employment_action IN ('HIRED', 'FIRED'));