alter table employees
add cardNr NUMBER;

alter table employees
    add constraint emp_card_fk
    foreign key (cardNr) REFERENCES pay (cardNr);