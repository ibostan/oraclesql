create or replace trigger logs_trigger
 after
 insert or delete
    on employees
    for each row
 declare
   l_transaction VARCHAR2(10);
    begin
    l_transaction := CASE
         WHEN INSERTING THEN 'INSERT'
         WHEN DELETING THEN 'DELETE'
    end;
if l_transaction = 'INSERT' then INSERT_LOGS();
else delete_logs();
end if;
end;
