alter table locations
add department_amount NUMBER;

comment on table locations is 'Contains the amount of departments in the location';