create table assignments (
    assignment_id NUMBER primary key,
    start_date date,
    end_date date,
    employee_id NUMBER,
    project_id NUMBER,
    constraint project_interval check (end_date > start_date)
);