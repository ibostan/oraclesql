create table pay (
    cardNr NUMBER,
    salary NUMBER(8, 2),
    commission_pct NUMBER(2, 2)
);