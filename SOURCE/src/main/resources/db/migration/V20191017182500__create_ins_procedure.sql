create or replace procedure insert_logs AS
    emp_id     employees.employee_id%type;
    emp_fname  employees.first_name%type;
    emp_lname  employees.last_name%type;
    emp_hire   employees.hire_date%type;
begin

    select employee_id, first_name, last_name, hire_date
    INTO emp_id, emp_fname, emp_lname, emp_hire
    FROM employees
    where employee_id = emp_id;

    insert into employment_logs (
        employment_id,
        first_name,
        last_name,
        employment_action,
        employment_status_timestamp)
        values
        (
        emp_id,
        emp_fname,
        emp_lname,
        'hired',
        emp_hire
        );
end insert_logs;